define([], function(){
	
	function ModelView(el,options){
		this.model = options.model;

		this.model.on('change',function(prop,value){
			this.data[prop] = value;
			this.render();
		})
	}
	

	return ModelView;
})