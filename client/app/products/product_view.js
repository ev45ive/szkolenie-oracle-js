define(['../abstract_view'], function(AbstractView){

	/**
	 * Product View
	 * @constructor ProductView
	 * @param {DOMElement} el - The title of the book.
	 * @param {ViewOptions} options - The author of the book.
	 */
	function ProductView (el, options) {
		AbstractView.call(this, el,options)
	}
	ProductView.prototype = Object.create(AbstractView.prototype);
	Object.assign(ProductView.prototype, {
		/**
		 * template
		 */
		template: function(data){
			return '<div>'+data.name+'</div>';
		}
	});
	return ProductView;
})