define(['../list_view','./product_view','./discounted_product_view'], function(ListView, ProductView){
	
		function ProductsListView(el,options){
			ListView.apply(this,arguments);
		}

		ProductsListView.prototype = Object.create(ListView.prototype);
		Object.assign(ProductsListView.prototype,{
			//...
			viewClass: ProductView
		})

		return ProductsListView;
})