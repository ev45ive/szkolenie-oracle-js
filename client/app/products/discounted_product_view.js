define(['jquery','products/product_view'], function($, ProductView){
	function DiscountProductView(el,options){
		ProductView.apply(this,arguments);
	}
	DiscountProductView.prototype = Object.create(ProductView.prototype);
	Object.assign(DiscountProductView.prototype,{
		template: function(data){
			return '<div>'+data.name+' Promo Price! Dicount: '+data.discount+' </div>';
		}
	});
	return DiscountProductView;
});