define(['./abstract_view'], function(AbstractView){
	
		function ListView(el,options){
			AbstractView.apply(this,arguments);
			this.viewClass = options.viewClass || this.viewClass;
		}

		ListView.prototype = Object.create(AbstractView.prototype);
		Object.assign(ListView.prototype,{
			render: function(){
				this.el.html('');
				var items = this.data.map(function(dataItem, i){
					var itemOptions = {
						data: dataItem,
						index: i
					}
					var item = this.renderItem(null, itemOptions);
					this.el.append(item.el);
					return item;
				}.bind(this));
			},
			itemOptionsFactory: function(itemOptions){
				return {
					data: itemOptions.data,
					index: itemOptions.index
				}
			},
			viewClass: AbstractView,
			itemFactory: function(el, itemOptions){
				var options = this.itemOptionsFactory(itemOptions);
				// polymorfic: use as both function or static class constructor:
				var viewClass = typeof this.viewClass === 'function'? this.viewClass(el,itemOptions) : this.viewClass;
				
				return new viewClass(el,options);
			},
			renderItem: function(el, itemOptions){
				var product = this.itemFactory(el,itemOptions)
				product.render();
				return product;
			}
		})

		return ListView;
})