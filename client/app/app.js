"use strict";
//app.js

require.config({
	baseUrl: 'app',	
    paths: {
    	// load jquery as amd module
       //"jquery": "../bower_components/jquery/dist/jquery"
    },
    shim:{

    }
});
// register global jQuery object as a module
define('jquery',[], function () {
    return window.jQuery;
});

// bootstraping :
require(['jquery','products/product_view','products/discounted_product_view','products/products_list_view'], 
function($,ProductView, DiscountProductView, ProductsListView){
console.log('Ypds!!!!')
	$(function(){
		/*var productEl = $('.product').get(0);
		var product = new ProductView(  productEl,{
			data:{
				name: 'M4A1 Abrams',
				description: 'Versatile tool',
				price: 1.2
			}
		});
		product.render();

		var discountedEl = $('.product.discount').get(0);
		var discounted = new DiscountProductView(  discountedEl,{
			data:{
				name: 'F16 with Offset',
				description: 'DIY airfighter kit',
				price: 2.4,
				discount: -0.2
			}
		});
		discounted.render();*/

		var productListData = [
			{
				name: 'M4A1 Abrams',
				description: 'Versatile tool',
				price: 1.2
			},{
				name: 'F16 with Offset',
				description: 'DIY airfighter kit',
				price: 2.4,
				discount: -0.2
			}
		];

		var productListEl = $('.products');

		function provideProductView(el,options){
				return options.data.discount? DiscountProductView : ProductView;
		}

		var list = new ProductsListView(productListEl,{
			data: productListData,
			viewClass: provideProductView
		})
		list.render()
	});
});

	