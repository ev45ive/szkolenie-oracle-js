define([], function(){

	function Model(data, options){
		this.data = data || {}
		this.callbacks = {
			_change:[],
		}
	}
	Model.prototype = {
		get: function(name){
			return this.data[name];
		},
		set: function(name, value){
			this.data[name] = value;

			this._trigger(name,value);
		},
		_trigger: function(name,value){
			if(this.callbacks[name]){
				this.callbacks[name].forEach(function(cb){
					cb(name,value)
				});
			}
			this.callbacks._change.forEach(function(cb){
				cb(name,value)
			});
		},
		on: function(name, property, callback){
			if('undefined' === typeof callback){
				callback = property;
				this.callbacks._change.push(callback)
			}else{
				this.callbacks[property] = this.callbacks[property] || [];
				this.callbacks[property].push(callback)

			}

		}
	}


	return Model;
});

