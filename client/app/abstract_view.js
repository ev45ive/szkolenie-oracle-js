define(['jquery'], function($){
	
	function AbstractView(el,options){
		this.el = el || document.createElement('div');
		this.data = options.data || {};
		this.template = options.template || this.template;
	}
	AbstractView.prototype = {
		render: function(){
			$(this.el).html(this.template(this.data));
		},
		template: function(){
			throw 'Cannot render AbstractView directly'
		}
	}

	return AbstractView;
})