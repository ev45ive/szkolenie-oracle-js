define(['jquery','app/products/product_view'], function($,ProductView) {
 
   describe('Product View', function() {
		
		var el, product, options = {};

		function mockProduct(){
			el = document.createElement('div');
	   		product = new ProductView(el,options);
		}

		beforeEach(function(){
			mockProduct(el,options);
		})

		afterEach(function(){
			options = {};
		})

   		it('should be created', function(){
   			expect(product).toBeDefined();
   		})

   		it('should save options', function(){
   			expect(product.el).toBe(el);
   		})

   		it('should render template', function(){
   			product.template = function(){
   				return '<div>TEST_TEMPLATE</div>'
   			}
   			product.render();
   			expect(product.el.innerHTML)
   			.toMatch('TEST_TEMPLATE')
   		})


   		it('should render options.data in template', function(){
   			options.data = {
				test: 'TEST_DATA',
   			};
   			options.template = function(data){
   				return '<div>'+data.test+'</div>'
			}
			mockProduct(el,options);

   			product.render();
   			expect(product.el.innerHTML)
   			.toMatch(options.data.test)
		});
   });

});