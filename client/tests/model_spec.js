define(['app/model'],function(Model){
	
	describe('Model', function(){

		var model;
		beforeEach(function(){
			model = new Model();
		})

		it('should get and set data', function(){
			model.set('name', 'Test Set');
			expect(model.get('name')).toEqual('Test Set');
		})

		it('should notify on data change ', function(done){
			
			var test_property = "Test name";
			var test_value = "Test value";

			model.on('change', function(property, value){
				expect(property).toEqual(test_property)
				expect(value).toEqual(test_value)
				done();
			})
			model.set(test_property, test_value);
		})

		it('should notify multiple recievers', function(){

			// On model change set View 1 value
			var view_1_value = '';
			model.on('change', function(property, value){
				view_1_value = value;
			})

			// On model change set View 2 value
			var view_2_value = '';
			model.on('change', function(property, value){
				view_2_value = value;
			})

			expect(view_1_value).toEqual('');
			expect(view_2_value).toEqual('');

			model.set('name','TEST_VALUE');

			expect(view_1_value).toEqual('TEST_VALUE');
			expect(view_2_value).toEqual('TEST_VALUE');

		})


		it('should notify on property change ', function(){
			
			model.set('property_name', '')
			model.on('change','property_name', function(property, value){
				expect(value).toEqual('test')
			})
			model.set('property_name', 'test')

		});
	})

})